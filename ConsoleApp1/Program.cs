﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note FirstTest = new Note("Harry Potter", "J. K. Rowling");
            Note SecondTest = new Note("Gospodar prstenova", "John Ronald Reuel Tolkien");
            Note ThirdTest = new Note("Matrix", "Lana Wachowski, Lilly Wachowski");
            Notebook notebook = new Notebook();
            notebook.AddNote(FirstTest);
            notebook.AddNote(SecondTest);
            notebook.AddNote(ThirdTest);
            IAbstractIterator iterator = notebook.GetIterator();
            for (int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();

            }
            Console.WriteLine();

            iterator.First().Show();

            iterator = notebook.GetIterator();
            notebook.RemoveNote(ThirdTest);
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}