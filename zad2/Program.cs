﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product FirstTest = new Product("Sat", 2000);
            Product SecondTest = new Product("PC", 10000);
            Product ThirdTest = new Product("Zvaka", 0.5);
            Box box = new Box();
            box.AddProduct(FirstTest);
            box.AddProduct(SecondTest);
            box.AddProduct(ThirdTest);
            IAbstractIterator iterator = box.GetIterator();
            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                Console.WriteLine();
                iterator.Next();
            }
        }
    }
}
