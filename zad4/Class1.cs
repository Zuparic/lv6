﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4
{
    class BankAccount
    {
        private string OwnerName;
        private string OwnerAddress;
        private decimal Balance;
        public BankAccount(string OwnerName, string OwnerAddress, decimal Balance)
        {
            this.OwnerName = OwnerName;
            this.OwnerAddress = OwnerAddress;
            this.Balance = Balance;
        }
        public void ChangeOwnerAddress(string address)
        {
            this.OwnerAddress = address;
        }
        public void UpdateBalance(decimal amount) { this.Balance += amount; }
        public string ownerName { get { return this.OwnerName; } }
        public string ownerAddress { get { return this.OwnerAddress; } }
        public decimal balance { get { return this.Balance; } }

        public Memento StoreState()
        {
            return new Memento(this.OwnerName, this.OwnerAddress, this.Balance);
        }
        public void RestoreState(Memento previous)
        {
            this.OwnerName = previous.OwnerName;
            this.Balance = previous.Balance;
            this.OwnerAddress = previous.OwnerAddress;
        }
        public override string ToString()
        {
            return "Name: " + this.ownerName + "\nAddress: " + this.ownerAddress + "\nBalance: " + this.balance+"\n";
        }
    }
}

